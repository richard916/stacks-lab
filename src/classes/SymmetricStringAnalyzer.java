package classes;

public class SymmetricStringAnalyzer {
	private String s; 
	public SymmetricStringAnalyzer(String s) {
		this.s = s; 
	}

	/**
	 * Determines if the string s is symmetric
	 * @return true if it is; false, otherwise. 
	 */
	public boolean isValidContent() { 
		SLLStack<Character> newStack = new SLLStack<Character>(); 

		for (int i=0; i < s.length(); i++) {
			char letter = s.charAt(i);
			if (Character.isLetter(letter)) {

				if (Character.isUpperCase(letter)) {
					newStack.push(letter);
				}
				else if (newStack.isEmpty()) {
					return false; 
				}
				else {
					char top = newStack.top();
					if (Character.toUpperCase(letter) == top) {
						newStack.pop();  
					}
					else {
						return false; 
					}
				}
			}
			else 
				return false; 
		} 

		if(!newStack.isEmpty()) {return false;}

		return true; 
	}

	public String toString() { 
		return s; 
	}

	public String parenthesizedExpression() throws StringIsNotSymmetricException {

		String result = "";

		if(!this.isValidContent()) {
			throw new StringIsNotSymmetricException("String is not symmetric.");
		}

		for (int i=0; i < s.length(); i++) { 
			char letter = s.charAt(i);
			
			if (Character.isLetter(letter)) {
				
				if (Character.isUpperCase(letter)) {
					result += " < " + letter + " ";
				}
				else {
					result += " " + letter + " > "; 
				}
			}
		} 
		return result;  
	}

}
